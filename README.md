# EChartsServer

#### 介绍
通过NodeJS生成ECharts图片

推荐使用pnpm安装依赖包，性能好速度快

#### 软件架构
使用express接收请求返回响应

使用echarts绘制图片

#### 更新记录

| 日期 | 更新 |
| -- | -- |
| 2023-11-20 | 新增Dockerfile，因为Canvas依赖的node-gyp不好安装，索性直接用Docker解决


#### 安装教程

1.  安装Canvas所需要的gyp所需要的依赖

本项目不同于其他Node项目，需要调用本机接口去生成图片，因此除了安装项目相关依赖，还需要安装一些系统依赖

详情可看[这里](https://www.npmjs.com/package/canvas)

考虑到Windows开发测试用得多，简单说一下Windows下的依赖

Canvas需要用到node-gyp，因此需要安装node-gyp的依赖

node-gpy在Windows上依赖两个程序：Python和VC++编译程序

Python可以直接在官网下载，Python3的版本就行，需要注册到环境变量中

VC++编译程序不是很好安装，最简单的方式是下载VS（不是VS Code），然后安装选项中选择`使用C++的桌面开发`这一项，安装完成后也附带了VC++编译程序，此方法最简单

安装好Python和VC++编译程序后在安装依赖即可，一般就不会失败了，否则可能会出现安装node-pre-gyp失败的情况


2.  安装依赖的node包
```shell
# 推荐使用pnpm，pnpm的性能真的很好
pnpm install
# 或者
npm install
# 或者
yarn
# 或者
tyarn

```

#### 使用说明

1.  使用Node启动即可app.js即可，也可以使用npm run start启动
2.  默认端口是3000，通过post请求根路径，传递echarts的Options即可
3.  使用post请求，将echarts的options作为body传递过来即可
4.  参数中扩展了width、height、pixelRationNumber，分别控制了宽度、高度和转为base64时的缩放倍率
4.  渲染的核心方法是`function generateImage()`

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

