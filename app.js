const { createCanvas } = require('canvas');
const express = require('express')
const echarts  = require("echarts");
const app = express()
const port = 3000;

// 根据官方文档5.3.0以后版本不需要这句，实测删了会报错
echarts.setCanvasCreator(() => {
  return createCanvas();
});

function generateImage(options,width,height,pixelRationNumber) {
  
  // 初始化canvas对象
  let canvas = createCanvas(width, height);

  // 初始化echarts对象
  const chart = echarts.init(canvas);

  // 禁用动画
  options.animation = false;
  
  // 配置echarts渲染参数
  chart.setOption(options, true);
  
  // 导出echarts图片为base64
  let base64 = chart.getDataURL({
    type: "png",
    pixelRatio: pixelRationNumber,
  });
  
  // 销毁echarts实例，避免内存泄漏
  chart.dispose();
  canvas = null;
  return base64;
}

app.use(express.json({ limit: "50mb" }));

app.post("/", (req, res) => {
  let options = req.query.options || req.body;

  // 如果接收到参数是字符串就先转为json
  if (typeof options === "string") {
    options = JSON.parse(options);
  }

  // 预定义宽高和缩放倍率
  let width = 600;
  let height = 400;
  let pixelRationNumber = 3;

  // 获取参数中的宽度
  if (isFinite(options.serviceWidth)) {
    width = options.serviceWidth;
    // 移除width属性
    delete options.serviceWidth;
  }
  // 获取参数中的高度
  if (isFinite(options.serviceHeight)) {
    height = options.serviceHeight;
    // 移除高度属性
    delete options.serviceHeight;
  }

  // 获取参数中的缩放倍率
  if (isFinite(options.servicePixelRationNumber)) {
    pixelRationNumber = options.servicePixelRationNumber;
    // 移除缩放倍率
    delete options.servicePixelRationNumber;
  }

  // 设置响应头
  // res.header("Content-Type", "image/png");
  // 返回base64格式图片
  var base64 =generateImage(options,width,height,pixelRationNumber);
  res.send(base64);
});

app.listen(port);
