FROM node:18
WORKDIR /app
COPY app.js package.json /app/
# RUN npm install
RUN apt update && apt install -y ttf-wqy-microhei ttf-wqy-zenhei xfonts-wqy xfonts-intl-chinese fonts-arphic-uming fonts-noto
RUN npm install

# COPY . .

EXPOSE 3000

CMD ["npm", "start"]